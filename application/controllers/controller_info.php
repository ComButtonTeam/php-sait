<?php

class Controller_Info extends Controller
{
	function __construct()
	{
		$this->model = new Model_Info();
		$this->view = new View();
		
	}

	function action_index()
	{	
		$data = $this->model->get_data();
		$this->view->generate('info_view.php', 'template_modul2_view.php', $data);
	}
	
	function action_page($param){
		
	}
	
	function action_category($param){
		$data = $this->model->get_category($param);
		$this->view->generate('info_view.php', 'template_modul2_view.php', $data);
	}
	
	function action_news($param){
		$data = $this->model->get_news($param);
		$this->view->generate('info_view.php', 'template_modul2_view.php', $data);
	}
}