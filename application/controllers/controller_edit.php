<?php

class Controller_Edit extends Controller
{	

	function __construct()
	{
		$this->model = new Model_Edit();
		$this->view = new View();
		
	}

	function action_news($param)
	{	
		$data = $this->model->get_news($param);
		$this->view->generate('edit_view.php', 'template_modul2_view.php', $data);
	}

	function action_posts($param)
	{	
		$data = $this->model->get_posts($param);
		$this->view->generate('edit_view.php', 'template_modul2_view.php', $data);
	}	
	
}