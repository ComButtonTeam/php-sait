<?php

class Controller_Dashboard extends Controller
{
	function action_index()
	{
		$this-view->render('dashboard/index');
	}
	
	public function __construct() {
		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');
		if($logged == false) {
		Session::destroy();
		header('Location: ../login');
		exit();
		}
	}
  public function logout() {
	Session::destroy();
	header('Location: ../login');
	exit();
	}	
	
	
}
