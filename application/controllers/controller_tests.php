<?php

class Controller_Tests extends Controller
{
	function __construct()
	{
		$this->model = new Model_Tests();
		$this->view = new View();
		
	}

	function action_index()
	{	
		$data = $this->model->get_data();
		$this->view->generate('tests_view.php', 'template_modul2_view.php', $data);
	}
	
	function action_page($param){
		
	}
	
	function action_category($param){
		$data = $this->model->get_category($param);
		$this->view->generate('tests_view.php', 'template_modul2_view.php', $data);
	}
	
	function action_test($param){
		$data = $this->model->get_test($param);
		$this->view->generate('tests_view.php', 'template_modul2_view.php', $data);
	}
}