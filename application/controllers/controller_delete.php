<?php

class Controller_Delete extends Controller
{
	function __construct()
	{
		$this->model = new Model_Delete();
		$this->view = new View();		
	}
	
	function action_delnews()
	{
		$this->model->deletenews($_POST["idnews"]);			
		$this->view->generate('info_view.php', 'template_modul2_view.php');		
	}

		function action_delpost()
	{
		$this->model->deletepost($_POST["idpost"]);			
		$this->view->generate('posts_view.php', 'template_modul2_view.php');		
	}
	
}
