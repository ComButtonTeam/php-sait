<?php

class Controller_Add extends Controller
{
	function __construct()
	{
		$this->model = new Model_Add();
		$this->view = new View();
		
	}
	
	function action_addnews()
	{
		if(!($_SERVER['REQUEST_METHOD'] === 'POST'))  {			
			$this->view->generate('add_view.php', 'template_modul2_view.php');
		} else {
			$this->model->insert_news($_POST['name'], $_SESSION['login'], $_POST['shortcontent'], $_POST['fullcontent']);			
			$this->view->generate('info_view.php', 'template_modul2_view.php');
		}
	}	
	function action_addposts()
	{
		if(!($_SERVER['REQUEST_METHOD'] === 'POST'))  {			
			$this->view->generate('add_view.php', 'template_modul2_view.php');
		} else {
			$this->model->insert_post($_POST['name'], $_SESSION['login'], $_POST['text'], $_POST['fulltext'], 23-02-02);			
			$this->view->generate('posts_view.php', 'template_modul2_view.php');
		}
	}	
	function action_addtests()
	{
		if(!($_SERVER['REQUEST_METHOD'] === 'POST'))  {			
			$this->view->generate('add_view.php', 'template_modul2_view.php');
		} else {
			$this->model->insert_post($_POST['name'], $_POST['login'], $_POST['password'], $_POST['r_pass'], $_POST['email'], $_POST['country']);			
			$this->view->generate('tests_view.php', 'template_modul2_view.php');
		}
	}
}
