<?php

class Controller_Registration extends Controller
{
	function __construct()
	{
		$this->model = new Model_Registration();
		$this->view = new View();
		
	}
	
	function action_index()
	{
		if(!($_SERVER['REQUEST_METHOD'] === 'POST'))  {			
			$this->view->generate('registration_view.php', 'template_view.php');
		} else {
			$this->model->insert_user($_POST['name'], $_POST['login'], $_POST['password'], $_POST['r_pass'], $_POST['email'], $_POST['country']);
			
			$this->view->generate('main_view.php', 'template_view.php');
		}
	}
}
