<?php

class Controller_Posts extends Controller
{
	function __construct()
	{
		$this->model = new Model_Posts();
		$this->view = new View();
		
	}

	function action_index()
	{	
		$data = $this->model->get_data();
		$this->view->generate('posts_view.php', 'template_modul2_view.php', $data);
	}
	
	function action_page($param){
		
	}
	
	function action_category($param){
		$data = $this->model->get_category($param);
		$this->view->generate('posts_view.php', 'template_modul2_view.php', $data);
	}
	
	function action_post($param){
		$data = $this->model->get_post($param);
		$this->view->generate('posts_view.php', 'template_modul2_view.php', $data);
	}
}