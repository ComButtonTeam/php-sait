<?php

class Controller_Login extends Controller
{	
	function __construct()
	{
		$this->model = new Model_Login();
		$this->view = new View();
		
	}
	
	function action_index()
	{
		if(!($_SERVER['REQUEST_METHOD'] === 'POST'))  {			
			$this->view->generate('login_view.php', 'template_view.php');
		} else {
			$this->model->run();			
		}
	}
		
}
