<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>English</title>
<link href="/resource/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/resource/script/CLEditor/jquery.cleditor.css" />
<script src="/resource/js/jquery-2.1.3.min.js"></script>
<script src="/resource/script/CLEditor/jquery.cleditor.min.js"></script>
<script>
$(document).ready(function () { $("#input").cleditor(); });
$(document).ready(function () { $("#input1").cleditor(); });
</script>

</head>
<body>
	<div id="templatemo_container">
    	<div id="templatmeo_header">
        	<div id="templatemo_menu">
            	<ul>
                    <li><a href="/info" class="current">Новости</a></li>
                    <li><a href="/posts" target="_parent">Статьи</a></li>
                    <li><a href="/materials" >Учебные материалы</a></li>
                    <li><a href="/tests">Тесты</a></li>					
					<?php
							echo '<li><a>Это вы: '.$_SESSION['login'].'</a></li>';
						?>	
					<li><a href="/main">Возврат</a></li>
                </ul> 
            </div>            
        </div><!-- End Of header -->
		
        
        <?php include 'application/views/'.$content_view; ?>
		
    	
        <div id="templatemo_bottom_bottom">
        </div>
        
        <div id="templatemo_footer">
        Copyright © 2015 <a href="#">Button Company Inc.</a> | 
        Designed by <a href="#" target="_parent">Button Company Inc.</a>       
    </div><!-- End Of Container -->
  

</body>
</html>