<!--SubPage Toprow Begin-->
<div id="toprowsub">
  <div class="center">
    <h2>About Us</h2>
  </div>
</div>
<!--Toprow END-->
<!--SubPage MiddleRow Begin-->
<div id="midrow">
  <div class="center">
    <div class="textbox2">
      <p><a>Мы студенты СТГУ МФПИТ группы б1ИФСТ21
      Портал школы английского разработан коммандой Button Company по заказу преподавателей по предмету "Интерактивные веб-приложения". Button Company состоит из 3х разработчиков: Королёв Константин, Мальцев Андрей, Ряснов Алексей.
      Сайт состоит из 2х частей. Первая часть несет информационную функцию, для того, держать в курсе последних событий всех желающих учиться в школе английского языка. Вторая часть представляет собой онлайн курсы, где обучающиеся найдут обучающие статьи, материалы, тесты.
      </a></p>
    </div>
  </div>
</div>
<!--MiddleRow END-->
<!--SubPage BottomRow Begin-->
<div id="bottomrow">
  <div id="box2holder">
    <h1>Наша команда</h1>
    <div class="box2"> <img alt="" src="/resource/images/leha.jpg" width="190" height="120" />
      <h3>Ряснов Алексей</h3>
      <p><a>Студент СГТУ б1ИФСТ21.</a></p>
    </div>
    <div class="box2"> <img alt="" src="/resource/images/kostia.jpg" width="190" height="120" />
      <h3>Королёв Константин</h3>
      <p><a>Студент СГТУ б1ИФСТ21.</a></p>
    </div>
    <div class="box2"> <img alt="" src="/resource/images/andrew.jpg" width="190" height="120" />
      <h3>Мальцев Андрей</h3>
      <p><a>Студент СГТУ б1ИФСТ21.</a></p>
    </div>    
  </div>
</div>
<!--BottomRow END-->
