<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>English</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="shortcut icon" href="/resource/images/favicon.gif" />
<link rel="stylesheet" type="text/css" media="screen" href="/resource/css/style.css" />


</head>	
	<body>
			<!--Header_Begin-->
			<div id="header">
				<div class="center">
				<div id="logo"><a href="/main"></a></div>
				<!--Menu_Begin-->
				<div id="menu">
					<ul>
						<li><a class="active" href="/main"><span>Главная</span></a></li>
						<li><a href="/about"><span>О нас</span></a></li>					
						<li><a href="/contacts"><span>Контакты</span></a></li>					
						<?php if(isset($_SESSION['status'])){							
								echo '<div class="user"> 
										<img src="/resource/images/noavatar.jpg">
										<ul class="navigation">								
												<li><a title="User">'.$_SESSION["login"].'</a>
													<ul>
														<li><a href="/profile" title="Profile">Профиль</a></li>
														<li><a href="/info" title="Write">Образование</a></li>
														<li><a href="/out" title="SEO">Выход</a></li>													
													</ul>
												</li>								
											</ul>
									</div>';
								
								
							}else{
								echo "<li><a href='/registration'><span>Регистрация</span></a></li>";
								echo "<li><a href='/login'><span>Вход</span></a></li>";
							}
						?>			
					</ul>
				</div>
				<!--Menu_End-->
				</div>
			</div>
			<!--Header_End-->					
			<?php include 'application/views/'.$content_view; ?>				
			<div id="footer">
				<div class="foot"> <span>Created</span> by <a href="">Button Company Inc.</a> is licensed under a <a href="#">Super Global World License.</a> </div>
			</div>
					
	</body>
</html>