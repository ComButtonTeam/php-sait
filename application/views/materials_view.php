<?php
if(!isset($_SESSION['status'])) 
{   
	echo "<script>window.location.replace('/404')</script>";        
}
?>

<div class="category_panel">
	<a href="/materials">Все категории</a>
	<a href="/materials/category/1">Учебники</a>
	<a href="/materials/category/2">Книги</a>
	<a href="/materials/category/3">Тренинги</a>
</div>

<div id="templatemo_content">
        	<div id="templatemo_content_top">
            	<div id="templatemo_content_bottom">
                	<div id="templaetmo_left">
					Тут когда-нибудь будут располагаться все материалы, очень полезные обучающимся. Чтоб они знали все иностранный язык.
                        <div class="cleaner"></div>
                    </div><!-- End Of Left-->                   
                    <div id="templatemo_right">                                
                        <div id="templatemo_right_bottom">
                        </div>
                    </div><!-- End Of Right-->
					<div id="templatemo_right">
                    	<div id="templatemo_right_top">                        
                        	<h1>Разделы:</h1>
                        </div>     
                        <div id="templatemo_right_mid">
                        	<div id="right_mid_top">
								<a href="/info"><img src="/resource/images/infomodul/templatemo_img_4.jpg" alt="Новости" width="293" height="89" /></a>
                                <a href="/posts"><img src="/resource/images/infomodul/templatemo_img_3.jpg" alt="Статьи" width="292" height="89" /></a>
                                <a href="/materials"><img src="/resource/images/infomodul/templatemo_img_5.jpg" alt="Материалы" width="293" height="89" /></a>
								<a href="/tests"><img src="/resource/images/infomodul/templatemo_img_6.jpg" alt="Тесты" width="293" height="89" /></a>                                
							</div>
						</div>               
                        <div id="templatemo_right_bottom">
                        </div>
                    </div><!-- End Of Right-->
                    
                    <div class="cleaner"></div>
                </div><!-- End Of Content Bottom-->
            </div><!-- End Of Content Top-->
       </div>	<!-- End Of Content-->