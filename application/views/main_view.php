﻿<!--Toprow Begin-->
<div id="page-wrap">		
		<div id="slider">
			<div id="mover">		
				<div id="slide-1" class="slide">				
					<h1>О нас</h1>					
					<p>Наша школа предоставляет наиболее полные условия для получения великолепного лингвистического образования.Хотите научиться языку быстро,качественно и без чрезмерных усилий по поиску информации?Вам к нам.<br/>
					<?php	
					if(!isset($_SESSION['status'])){
						echo '<a href="/registration" ><span class="btnclass">Зарегистрироваться</span></a></p>';
					}					
					?>				
					
					<img src="/resource/images/slider_img/slide1.jpg" alt="learn more" />					
				</div>				
				<div class="slide">				
					<h1>Тестирование</h1>					
					<p>Вы можете пройти онлайн тест и проверить свой уровень знаний.Хотите?<br/>
					<?php	
					if(isset($_SESSION['status'])){
						echo '</p><a href="/test"><span class="btnclass">Тестироваться</span></a></p>';
					}
					else{						
						echo '<p><a href="/login" ><span class="btnclass">Тестироваться</span></a></p>';
					}
					?>
					
					
					<img src="/resource/images/slider_img/slide2.png" alt="learn more" />					
				</div>				
				<div class="slide">				
					<h1>Обратная связь</h1>					
					<p>Все свои недовольства,пожелания и замечания отправлять по адресу:Одесса,третья маца-пекарня,в письменном виде.</p>					
					<img src="/resource/images/slider_img/slide32.png" alt="learn more" />					
				</div>			
			</div>		
		</div>		
	</div>	
<!--Toprow END-->

<!--MiddleRow Begin-->
<div id="midrow">
  <div id="container">
    <div class="box">
      <h1>План обучения</h1>
      <a class="plan" href="#">План обучения</a>
	 
      <p><a>Наша школа предоставляет гибкий план обучения. Мы предоставляем как очные так и заочные курсы. Если в вашем городе нет нашего филиала обучения, то вы можете воспользоваться онлайн сервисом и получить качественное образование в вашем городе.</a><br />
        <br />
        <a href="#" class="button"><span>Подробнее</span></a></p>
    </div>
    <div class="box">
      <h1>Почему вы выбрали нас?</h1>
      <a class="whyus" href="#">Почему вы выбрали нас?</a>
      <p><a>Мы - молодая и бурно развивающаяся компания. Мы содержим штат из высоко кваллифицированных специалистов. Наши онлайн курсы разработаны по уникальной методике, благодаря которым скорость обучения возрастает на 60 - 80%.</a><br />
        <br />
        <a href="#" class="button"><span>Подробнее</span></a></p>
    </div>
    <div class="box last">
      <h1>Общение</h1>
      <a class="support" href="#">Общение</a>
      <p><a>Наш образовательный портал предоставляет возможность общения с носителями языка, что как известно является самым эффективным способом обучения. Выбирайте себе собеседника, заводите друзей из других стран. Проводите время с пользой и удовольствием.</a><br />
        <br />
        <a href="#" class="button"><span>Подробнее</span></a></p>
    </div>
  </div>
</div>
<!--MiddleRow END-->
<!--BottomRow Begin-->
<div id="bottomrow">
  <div class="textbox">
    <h1>Дополнительная информация</h1>
    <a>Вы также можете быть постоянно в курсе событий используя сообщества и группы в социальных сетях.</a> </div>
  <div class="feed"> <a href="https://twitter.com/"><img alt="" src="/resource/images/twitter.jpg" height="80" width="75" /></a> <a href="#"><img alt="" src="resource/images/rss.jpg" height="80" width="67" /></a> </div>
</div>
<!--BottomRow END-->
<script type="text/javascript" src="resource/js/jquery-1.2.6.js"></script>
<script type="text/javascript" src="resource/js/startstop-slider.js"></script>