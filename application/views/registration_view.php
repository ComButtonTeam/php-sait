﻿<?php
if(isset($_SESSION['status'])) 
{    
	echo "<script>window.location.replace('/404')</script>";    
}
?>
<!--SubPage Toprow Begin-->
<div id="toprowsub">
  <div class="center">
    <h2>Регистрация</h2>
  </div>
</div>
<!--Toprow END-->
<!--SubPage MiddleRow Begin-->
<div id="midrow">
  <div class="center">  
    <div class="textbox2"><br/>
		<div class="logreg_container">
			<p>Внесите свои данные</p>
			<form action="/registration" id="reg" method="POST">
				<p><input type="text" name="login" pattern="^[A-Za-zА-Яа-яЁё]+$" placeholder="Логин" required><span></span></p>
				<p><input type="text" name="name" pattern="^[A-Za-zА-Яа-яЁё]+$" placeholder="Имя" required><span></span></p>
				<p><input type="text" name="country" pattern="^[A-Za-zА-Яа-яЁё]+$" placeholder="Город" required><span></span></p>
				<p><input type="email" name="email" placeholder="Email"required><span></span></p>
				<p><input type="password" name="password" placeholder="Пароль" required><span></span></p>
				<p><input type="password" name="r_pass" placeholder="Повторите пароль" required><span></span></p><br />				
				<button type="submit" form="reg" name="submit" value="submit">Регистрация</button>
			</form>
		</div>
		
    </div>
  </div>
</div>

<!--MiddleRow END-->
